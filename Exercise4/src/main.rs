use std::sync::mpsc;
use std::thread;
use std::time::Duration;

const THREADS: u32 = 10;

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();

    for thread_num in 0..THREADS {
        let txc = mpsc::Sender::clone(&tx);
        thread::spawn(move || {
            for msg_num in 0..3 {
                txc.send(format!("Thread #{} msg {}", thread_num, msg_num)).expect("Failed to transmit.");
                thread::sleep(Duration::from_secs(1));
            }
        });
    }

    for received in rx {
        println!("Got: {}", received);
    }
}

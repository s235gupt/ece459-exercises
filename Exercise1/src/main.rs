// You should implement the following function:

fn sum_of_multiples(mut number: i32, multiple1: i32, multiple2: i32) -> i32
{
    let mut ret = 0;
    while number > 0 {
        number -= 1;
        if number % multiple1 == 0 || number % multiple2 == 0 {
            ret += number;
        }
    }
    ret
}

fn main() {
    println!("{}", sum_of_multiples(1000, 5, 3));
}

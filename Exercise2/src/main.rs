// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    match n {
        0 => 0,
        1 => 1,
        _ => fibonacci_number(n - 1) + fibonacci_number(n - 2)
    }
}


fn main() {
    println!("{}", fibonacci_number(10));
}

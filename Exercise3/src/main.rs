use std::thread;

static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    let mut children = vec![];

    for _ in 0..N {
        children.push(
            thread::spawn(|| {
                println!("Hello from a thread!");
            })
        );
    }

    println!("Now we wait for the threads to finish.");

    for thread in children {
        thread.join().unwrap();
    }

    println!("All threads finished!")
}
